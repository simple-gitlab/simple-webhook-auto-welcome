const port = 8081
const token = ''

const express = require('express')
const bodyParser = require('body-parser');
const { Gitlab } = require('@gitbeaker/node');

const gitlab = new Gitlab({token});
const app = express()
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.post('/merge_request', function (req, resp) {
  console.log(req.body)
  const {
    event_type,
    user: {
      username
    },
    object_attributes: {
      title,
      iid: mergeRequestId
    },
    project: {
      id: projectId,
    }
  } = req.body

  if ('merge_request' === event_type){
    const text = `hello <b>${username}</b>.<br/> I have received merge request about "${title}".`
    console.log(text)
    gitlab.MergeRequestNotes.create(projectId, mergeRequestId, text)
      .then(() => {
        resp.send("ok")
      })
      .catch(err =>{
        resp.send(err)
        resp.sendStatus(500)
    })
  }
})
app.listen(port, '',()=>{
  console.log(`http service listen: http://127.0.0.1:${port}/merge_request ...`)
})
