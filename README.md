## simple-webhook-auto-webhook

这是一个非常简单的 gitlab webhook 接收器。

当 gitlab 触发 Merge request events 时, 该工具自动在该 Merge request 下自动回复欢迎语。


### 开启服务
```
npm i
node index.js
```

### 注册服务

1. 根据上述开启服务。
2. 准备一个gitlab账号, 在浏览器登录后点击右上角 `Edit profile` 中找到 `Access Tokens`, 添加一个Token（Select scopes 至少选择api）, 将得到的Token添加到 index.js 的第二行。
3. 将该账号添加到一个项目中, 在该项目的左侧边栏中选择Settings > Webhooks。
4. 将开启服务后得到的URL输入（需要把ip换成正确的）, Trigger 仅勾选 `Merge request events`, 去除 `Enable SSL verification`, 点击 `Add webhook`。
5. 新建一个 `New merge request`，可以看到 [test-webhook](https://gitlab.com/simple-gitlab/test-project/-/merge_requests/1) 的欢迎语。


### 感谢

- [gitlab - webhook](https://www.yuque.com/mingzibaliao/git/twek7y)
